import pandas as pd
from docx import Document
import aspose.words as aw
import pandas_read_xml as pdx
import os
import subprocess
from tqdm import tqdm
import time


def get_tables(doc_file_path): # возвращает список датафреймов, полученных из таблиц файла .doc/.docx
    document = Document(doc_file_path)
    data = []
    dataframes = []
    for table in document.tables:
        table_data = []
        for row in table.rows:
            row_data = []
            for cell in row.cells:
                row_data.append(cell.text)
            table_data.append(row_data)
        data.append(table_data)
    for table in data:
        df = pd.DataFrame(table)
        dataframes.append(df)
    return dataframes
# print(get_tables("test.docx"))

def get_sheets(sheet_file_path): # NEW
    list_of_dfs = []
    df = pd.ExcelFile(sheet_file_path)
    for i in range(len(df.sheet_names)):
        sheet = df.parse(i)
        list_of_dfs.append(sheet)
    return list_of_dfs

def df_to_str_old(df): # конкатенация всех ячеек датафрейма в строку
    table_string = ""
    for rowIndex, row in df.iterrows():
        for columnIndex, value in row.items():
            table_string = table_string + " " + str(value) # make sure headers are in the final string (partly done - def df_to_str_2)
    return table_string

def df_to_str(df): 
    table_string = ""
    for i in range (len(list(df))):
        table_string = table_string + str(list(df)[i]) + " "
    for rowIndex, row in df.iterrows():
        for columnIndex, value in row.items():
            table_string = table_string + " " + str(value) 
    return table_string

def any_to_dfs(doc_file_path): # берёт .doc/.docx/.xls/.xlsx, возвращает датафрейм / список датафреймов
    if doc_file_path.endswith((".docx", ".doc")):
        dataframes = get_tables(doc_file_path)
    if doc_file_path.endswith((".xls", ".xlsx")):
        dataframes = get_sheets(doc_file_path) # add a separate func for multiple sheets (done - def get_sheets)
    return dataframes

def get_tables_from_files(file_paths): # берёт массив доков, .doc/.docx/.xls/.xlsx, возвращает список датафреймов
    dfs_list = []
    for file_path in file_paths:
        df = any_to_dfs(file_path)
        dfs_list.extend(df)
    return dfs_list

def get_files(dir): # список файлов нужных форматов по папке-датасету
    list_of_paths = []
    for subdir, dirs, files in os.walk(dir):
        for file in files:
            if file.endswith((".docx", ".xls", ".xlsx")):
                path = os.path.join(subdir, file)
                list_of_paths.append(path)
    return list_of_paths

def convert2docx(dir):
    list_of_paths = []
    list_of_subdirs = []
    for subdir, dirs, files in os.walk(dir):
        for file in files:
            if file.endswith(".doc"):
                path = os.path.join(subdir, file)
                subprocess.run([
                    "soffice",
                    "--convert-to",
                    "docx",
                    "--outdir",
                    subdir,
                    path])
    print('done everything')

# convert2docx('E:\Scripts\disk\zakupki.goz_test')

def build_dataset(dir): # without content
    list_of_unique_paths = get_files(dir)
    list_of_paths = []
    list_of_ids = []
    list_of_tables = []
    list_of_sheets = []
    for path in tqdm(list_of_unique_paths):
        if path.endswith('.docx'):
            document = Document(path)
            for count, table in enumerate(document.tables):
                list_of_paths.append(path)
                list_of_tables.append(count)
                id = path.split("\\")[4]
                list_of_ids.append(id)
        elif path.endswith(('.xls', '.xlsx')): 
            list_of_sheets = get_sheets(path)
            for count, table in enumerate(list_of_sheets):
                list_of_paths.append(path)
                list_of_tables.append(count)
                id = path.split("\\")[4]
                list_of_ids.append(id)
    df = pd.DataFrame(dict(auction_id=list_of_ids, path=list_of_paths, table_id=list_of_tables))
    return df

def build_full_dataset(dir): # with content for each table
    list_of_unique_paths = get_files(dir)
    list_of_paths = []
    list_of_ids = []
    list_of_tables = []
    list_of_sheets = []
    list_with_content = []
    corrupted_path_list = []
    any_error_list = []
    index_error_counter = 0
    any_error_counter = 0
    for path in tqdm(list_of_unique_paths):
        try:
            if path.endswith('.docx'):
                document = Document(path)
                content = get_tables(path) # list with dataframe-tables from a doc
                for count, table in enumerate(document.tables):
                    list_of_paths.append(path)
                    list_of_tables.append(count)
                    id = path.split("\\")[4]
                    list_of_ids.append(id)
                    list_with_content.append(df_to_str(content[count]).replace("\n", " "))
            elif path.endswith(('.xls', '.xlsx')): 
                list_of_sheets = get_sheets(path) # list of dfs
                for count, table in enumerate(list_of_sheets):
                    list_of_paths.append(path)
                    list_of_tables.append(count)
                    id = path.split("\\")[4]
                    list_of_ids.append(id)
                    list_with_content.append(df_to_str(table).replace("\n", " "))
        except IndexError:
            index_error_counter += 1
            print(index_error_counter)
            corrupted_path_list.append(path)
        except:
            any_error_counter += 1
            print(any_error_counter)
            any_error_list.append(path)
    df = pd.DataFrame(dict(auction_id=list_of_ids, path=list_of_paths, table_id=list_of_tables, content=list_with_content))
    print('corrupted paths: ' + corrupted_path_list)
    print('corrupted paths with non-index errors: ' + any_error_list)
    return df


def build_test_dataset(dir): # with time
    list_of_unique_paths = get_files(dir)
    list_of_paths = []
    list_of_ids = []
    list_of_tables = []
    list_of_sheets = []
    list_with_content = []
    corrupted_path_list_with_index_errors = []
    corrupted_path_list_with_any_errors = []
    path_list_with_long_comp = []
    index_error_counter = 0
    any_error_counter = 0
    for path in tqdm(list_of_unique_paths):
        try:
            if path.endswith('.docx'):
                t = time.process_time()
                document = Document(path)
                content = get_tables(path) # list with dataframe-tables from a doc
                for count, table in enumerate(document.tables):
                    list_of_paths.append(path)
                    list_of_tables.append(count)
                    id = path.split("\\")[4]
                    list_of_ids.append(id)
                    list_with_content.append(df_to_str(content[count]).replace("\n", " "))
                elapsed_time = time.process_time() - t
                if elapsed_time > 60:
                    path_list_with_long_comp.append(path)
            elif path.endswith(('.xls', '.xlsx')): 
                t = time.process_time()
                list_of_sheets = get_sheets(path) # list of dfs
                for count, table in enumerate(list_of_sheets):
                    list_of_paths.append(path)
                    list_of_tables.append(count)
                    id = path.split("\\")[4]
                    list_of_ids.append(id)
                    list_with_content.append(df_to_str(table).replace("\n", " "))
                elapsed_time = time.process_time() - t
                if elapsed_time > 60:
                    path_list_with_long_comp.append(path)
        except IndexError:
            index_error_counter += 1
            print(index_error_counter)
            corrupted_path_list_with_index_errors.append(path)
        except:
            any_error_counter += 1
            print(any_error_counter)
            corrupted_path_list_with_any_errors.append(path)
    df = pd.DataFrame(dict(auction_id=list_of_ids, path=list_of_paths, table_id=list_of_tables, content=list_with_content))
    corrupted_path_index_df = pd.DataFrame(corrupted_path_list_with_index_errors)
    corrupted_path_index_df.to_csv('index_errors_paths.csv')
    corrupted_path_any_df = pd.DataFrame(corrupted_path_list_with_any_errors)
    corrupted_path_any_df.to_csv('any_errors_paths.csv')
    long_comp_df = pd.DataFrame(path_list_with_long_comp)
    long_comp_df.to_csv('long_comp_paths.csv')
    return df


# dir = 'E:\Scripts\disk\zakupki.goz'
# data = build_full_dataset(dir)
# data.to_csv('dataset_with_content.csv', sep='|')

# if __name__ == "__main__":